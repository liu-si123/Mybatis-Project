package cn.oa.service.impl;

import cn.oa.dao.DepartmentDao;
import cn.oa.entity.Department;
import cn.oa.service.DepartmentService;
import cn.oa.util.SessionFatoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @Description TODO
 * @Author hyl
 * @Date 2022/3/29 9:06
 **/
public class DepartmentServiceImpl implements DepartmentService {
	SqlSessionFactory sessionFactory = SessionFatoryUtil.sessionFactory;
	@Override
	public List<Department> getAllDepartments() throws SQLException {
		SqlSession sqlSession = sessionFactory.openSession();

		DepartmentDao departmentDao = sqlSession.getMapper(DepartmentDao.class);
		List<Department> list = departmentDao.getAllDepartments();

		sqlSession.close();
		return list;
	}
}
