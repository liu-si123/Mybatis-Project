package cn.oa.service.impl;

import cn.oa.dao.EmployeeDao;
import cn.oa.entity.Employee;
import cn.oa.entity.EmployeeParamVo;
import cn.oa.service.EmployeeService;
import cn.oa.util.Page;
import cn.oa.util.SessionFatoryUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author hyl
 * @Date 2022/3/29 9:06
 **/
public class EmployeeServiceImpl implements EmployeeService {
	SqlSessionFactory sessionFactory = SessionFatoryUtil.sessionFactory;

	@Override
	public Employee findEmployee(String employeeNo, String password) throws SQLException {
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
		Employee tmp =  employeeDao.findEmployee(employeeNo, password);
		sqlSession.close();
		return tmp;
	}

	@Override
	public void getEmployeesByPage(String name, Page pageObj) throws SQLException {
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);

		EmployeeParamVo vo = new EmployeeParamVo();
		vo.setName(name);
		vo.setBeginIndex(pageObj.getBeginIndex());
		vo.setPageSize(pageObj.getPageSize());

		try {
			int totalCount = employeeDao.getEmployeesCount(vo);
			pageObj.setTotalCount(totalCount);
			if (totalCount > 0) {
				if (pageObj.getCurrPageNo() > pageObj.getTotalPageCount()) {
					pageObj.setCurrPageNo(pageObj.getTotalPageCount());
				}
				List<Employee> empList = employeeDao.getEmployeesByPage(vo);
				pageObj.setEmployeeList(empList);
			} else {
				pageObj.setCurrPageNo(0);
				pageObj.setEmployeeList(new ArrayList<Employee>());
			}

		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}finally{
			sqlSession.close();
		}
	}

	@Override
	public int updateEmployee(Employee employee) throws SQLException {
		int ret=0;
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
        ret =employeeDao.updateEmployee(employee);
        sqlSession.commit();
		sqlSession.close();
		sqlSession.rollback();
		return ret;
	}

	@Override
	public int addNewEmployee(Employee employee) throws SQLException {
		int ret=0;
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
		ret =employeeDao.addNewEmployee(employee);
		sqlSession.commit();
		sqlSession.close();
		sqlSession.rollback();
		return ret;
	}

	@Override
	public Employee getEmployeeById(int id) throws SQLException {
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
		Employee employee =employeeDao.getEmployeeById(id);
		sqlSession.close();
		return employee;
	}

	@Override
	public int deleteEmployeeById(int id) throws SQLException {
		int ret=0;
		SqlSession sqlSession = sessionFactory.openSession();
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
		ret =employeeDao.deleteEmployeeById(id);
			sqlSession.commit();
			sqlSession.rollback();
		sqlSession.close();
		return ret;
	}

	@Override
	public int updatePassword(int id, String pwd) throws SQLException {
		Employee tmp = new Employee();
		tmp.setId(id);
		tmp.setPassword(pwd);
		return updateEmployee(tmp);
	}
}
