package cn.oa.service.impl;

import cn.oa.dao.AttendanceDao;
import cn.oa.entity.Attendance;
import cn.oa.entity.AttendanceParamVo;
import cn.oa.service.AttendanceService;
import cn.oa.util.Page;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author hyl
 * @Date 2022/3/29 9:05
 **/
public class AttendanceServiceImpl implements AttendanceService {
	AttendanceDao attendanceDao = null;
	SqlSession sqlSession = null;
	Attendance attendance = null;
	int ret = 0;

	@Override
	public int addSignInByEmployeeId(int employeeId, String date) throws SQLException {
		try {
			String resoure = "mybatis-config.xml";
			InputStream is = Resources.getResourceAsStream(resoure);
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			sqlSession = factory.openSession();
			attendanceDao = sqlSession.getMapper(AttendanceDao.class);
			attendance = attendanceDao.getRecordByEmployeeIdAndDate(employeeId, date);

			if (attendance == null) {
				ret = attendanceDao.addSinginByEmployeeId(employeeId);
			} else {
				ret = 0;
			}
			sqlSession.commit();
		} catch (IOException e) {
			e.printStackTrace();
			sqlSession.rollback();
		} finally {
			sqlSession.close();
		}
		return ret;
	}

	@Override
	public int addSignOutByEmployeeId(int employeeId, String date) throws SQLException {
		try {
			String resoure = "mybatis-config.xml";
			InputStream is = Resources.getResourceAsStream(resoure);
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			sqlSession = factory.openSession();
			attendanceDao = sqlSession.getMapper(AttendanceDao.class);
			attendance = attendanceDao.getRecordByEmployeeIdAndDate(employeeId, date);

			if (attendance == null) {
				ret = 0;
			} else {
				if (attendance.getSignOutTime() == null) {
					ret = attendanceDao.addSinginByEmployeeId(attendance.getId());
				} else {
					ret = -2;
				}
			}
			sqlSession.commit();
		} catch (IOException e) {
			e.printStackTrace();
			sqlSession.rollback();
		} finally {
			sqlSession.close();
		}
		return ret;
	}

	@Override
	public void getAttendancesByPage(String name, String searchDate, Page pageObj) throws SQLException {
		AttendanceParamVo vo = new AttendanceParamVo();
		try {
			String resoure = "mybatis-config.xml";
			InputStream is = Resources.getResourceAsStream(resoure);
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			sqlSession = factory.openSession();
			attendanceDao = sqlSession.getMapper(AttendanceDao.class);


			vo.setName(name);
			vo.setSearchDate(searchDate);
			vo.setBeginIndex(pageObj.getBeginIndex());
			vo.setPageSize(pageObj.getPageSize());

			int totalCount = attendanceDao.getAttendancesCount(vo);
			pageObj.setTotalCount(totalCount);

			if (totalCount > 0) {
				if (totalCount > 0) {
					if (pageObj.getCurrPageNo() > pageObj.getTotalPageCount()) {
						pageObj.setCurrPageNo(pageObj.getTotalPageCount());
					}
					List<Attendance> list = attendanceDao.getAttendancesByPage(vo);
					pageObj.setAttendanceList(list);
				} else {
					pageObj.setCurrPageNo(0);
					pageObj.setAttendanceList(new ArrayList<Attendance>());
				}
			}
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
	}
}
