package cn.oa.service;

import cn.oa.entity.Attendance;
import cn.oa.entity.AttendanceParamVo;
import cn.oa.util.Page;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLException;
import java.util.List;


public interface AttendanceService {
    //员工上班打卡
    public int addSignInByEmployeeId(int employeeId, String date) throws SQLException;
    //员工下班打卡
    public int addSignOutByEmployeeId(int employeeId, String date) throws SQLException;
    //查询考勤记录
    public void getAttendancesByPage(String name, String searchDate, Page pageObj) throws SQLException;
  //  public void getAttendancesByPage(@Param("name") String name,@Param("searchDate")String searchDate, Page pageObj) throws SQLException;
//    public List<Attendance> getAttendancesByPage(AttendanceParamVo attendanceParamVo) throws SQLException;
}
