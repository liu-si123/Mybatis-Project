package cn.oa.util;

import cn.oa.entity.Attendance;
import cn.oa.entity.Employee;

import java.util.List;

public class Page {
    //总页数
    private int totalPageCount = 0;
    //页面大小，即每页显示记录数
    private int pageSize = 10;
    //记录总数
    private int totalCount;
    //当前页码
    private int currPageNo = 1;

    private int beginIndex;

    //每页员工集合
    private List<Employee> employeeList;

    //每页考勤记录集合
    private List<Attendance> attendanceList;

    public int getCurrPageNo() {
        if (totalPageCount == 0){
            return 0;
        }
        return currPageNo;
    }
    public void setCurrPageNo(int currPageNo) {
        if (currPageNo > 0){
            this.currPageNo = currPageNo;
        }
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        if (pageSize > 0){
            this.pageSize = pageSize;
        }
    }
    public int getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(int totalCount) {
        if (totalCount > 0){
            this.totalCount = totalCount;
            //计算总页数
            totalPageCount = this.totalCount % pageSize == 0 ?
                    (this.totalCount / pageSize) : (this.totalCount / pageSize + 1);
        }
    }
    public int getTotalPageCount() {
        return totalPageCount;
    }
    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public int getBeginIndex() {
        beginIndex = (currPageNo - 1) * pageSize;
        return beginIndex;
    }


}
