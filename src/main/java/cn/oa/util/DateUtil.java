package cn.oa.util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static String getFormatTime(Date date){
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.setTime(date);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");;
        return dateFormat.format(date);
    }

    public static String now(){
        Date d = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(d);
    }

    public static Date string2Date(String dateStr){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try{
            date = sdf.parse(dateStr);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return date;
    }
}
