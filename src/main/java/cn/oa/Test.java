package cn.oa;

import cn.oa.dao.EmployeeDao;
import cn.oa.entity.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * @Description TODO
 * @Author hyl
 * @Date 2022/3/29 11:11
 **/
public class Test {
	SqlSession sqlSession;

	@Before
	public void before(){
		InputStream is = null;
		try {
			is = Resources.getResourceAsStream("mybatis-config.xml");
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
			sqlSession =  factory.openSession();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@After
	public void after(){
		if (sqlSession!=null){
			sqlSession.close();
		}
	}

	@org.junit.Test
	public void testEmployee(){
		EmployeeDao employeeDao = sqlSession.getMapper(EmployeeDao.class);
		try {
			Employee tmp =  employeeDao.findEmployee("1000890901", "123456");
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
}
