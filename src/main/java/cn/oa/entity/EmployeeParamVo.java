package cn.oa.entity;

import java.util.Date;

/**
 * @Description 员工参数
 * @Author hyl
 * @Date 2022/3/29 10:05
 **/
public class EmployeeParamVo {
	private String name;  //员工姓名
	private Integer deptId ;//部门编号

	private Date beginTime; // 之前  100
	private Date endTime; //之后  200

	private Integer beginIndex;
	private Integer pageSize;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBeginIndex() {
		return beginIndex;
	}

	public void setBeginIndex(Integer beginIndex) {
		this.beginIndex = beginIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
}
