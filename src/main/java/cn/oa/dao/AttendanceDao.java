package cn.oa.dao;

import cn.oa.entity.Attendance;
import cn.oa.entity.AttendanceParamVo;
import cn.oa.entity.Employee;
import cn.oa.entity.EmployeeParamVo;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLException;
import java.util.List;

public interface AttendanceDao {
   //删除员工的所有考勤记录
   public int deleteAttendanceByEmployeeId(int employeeId) throws SQLException;
    //查找打卡记录
   public Attendance getRecordByEmployeeIdAndDate(@Param("id") int employeeId,@Param("time") String data) throws SQLException;
//    public Attendance getRecordByEmployeeIdAndDate(@Param("id") int employeeId, @Param("time") String date) throws SQLException;
    //上班打卡
    public int addSinginByEmployeeId(int employeeId) throws SQLException;
    //根据考勤id记录签退
    public int updateSingoutByAttendanceId(int id) throws SQLException;
    //获取考勤记录
    public List<Attendance> getAttendancesByPage(AttendanceParamVo vo) throws SQLException;
//    public List<Attendance> getAttendancesByPage(String name, String searchDate, int pageNo, int pageSize) throws SQLException;

    //获取记录总数
    public int getAttendancesCount(AttendanceParamVo vo) throws SQLException;

}
