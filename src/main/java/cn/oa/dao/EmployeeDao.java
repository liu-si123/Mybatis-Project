package cn.oa.dao;

import cn.oa.entity.Employee;
import cn.oa.entity.EmployeeParamVo;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeDao {
    //查找员工
    public Employee findEmployee(@Param("no") String employeeNo,@Param("pwd") String password) throws SQLException;
    //获取员工列表
    public List<Employee> getEmployeesByPage(EmployeeParamVo vo) throws SQLException;
    //获取员工总数
    public int getEmployeesCount(EmployeeParamVo vo) throws SQLException;
    //修改员工信息
    public int updateEmployee(Employee employee) throws SQLException;
    //新增员工
    public int addNewEmployee(Employee employee) throws SQLException;
    //根据ID获取员工信息
    public Employee getEmployeeById(int id) throws SQLException;
    //根据工号查询
    public int getEmployeeByNo(String employeeNo) throws SQLException;
    //删除员工
    public int deleteEmployeeById(int id) throws SQLException;
    //更新用户密码
    /*public int updatePasswordById(int id, String pwd) throws SQLException;*/
}
